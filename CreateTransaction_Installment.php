<?php


require_once 'moneyspace/Api.php';

$api = new Api();

$msdata = $api->CreatePayment([
    'firstname' => "test",
    'lastname' => "test",
    'email' => "stepclick@outlook.com",
    'phone' => "0888888888",
    'amount' => round("4800",2),
    'description' => "test123",
    'address' => "test/test",
    'message' => "test456",
    'feeType' => "include",
    'order_id' => "TEST-".date("YmdHis"),
    "payment_type" => "installment",
    'success_Url' => "https://www.moneyspace.net?status=success22",
    'fail_Url' => "https://www.moneyspace.net?status=fail22",
    'cancel_Url' => "https://www.moneyspace.net?status=cancel22",
    'agreement' => 1,
    'bankType' => "BAY",
    'startTerm' => "3",
    'endTerm' => "7"
]);


$response = json_decode($msdata);




?>


<!DOCTYPE html>
<html>
	<head>
		<title>Demo Pay</title>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	</head>
	<body>
        <div align="center">
        <div align="center">
            <div id="moneyspace-payment" 
            template="2"
            lang="th"
            ms-title="ผ่อนชำระเงิน : <?=$response[0]->transaction_ID?>" 
            ms-key="<?=$response[0]->mskey?>" 
            description="false"
            color=""
		    bgcolor="">
        </div>
        </div>

   
	<script type="text/javascript" src="https://a.moneyspace.net/js/moneyspace_payment.js"></script></body>
</html>