<?php

require_once 'moneyspace/Config.php';

class Api extends Config
{
  protected static $baseUrl = 'https://a.moneyspace.net';

  protected static $secret_id = null;

  protected static $secret_key = null;

  /**
   * @param string $secret_id
   * @param string $secret_key
   */

  public function __construct($secret_id = null, $secret_key = null)
  {
      self::$secret_id = $this->getSecret_id();
      self::$secret_key = $this->getSecret_key();
  }

  public function setBaseUrl($baseUrl)
  {
    self::$baseUrl = $baseUrl;
  }

  public static function getBaseUrl()
  {
    return self::$baseUrl;
  }

  public static function getSecretID()
  {
    return self::$secret_id;
  }

  public static function getSecretKey()
  {
    return self::$secret_key;
  }

  public function getHash($data, $key)
  {
    return hash_hmac('sha256', $data, $key);
  }

  public function getTime()
  {
    return date("YmdHis");
  }

  public function CallAPI($path, $param)
  {

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->getBaseUrl() . $path);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $data = $param;

    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $output = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);

    return $output;
  }

  public function CreatePayment($data)
  {
    $msdata =  array_merge($data, array('secret_id' => $this->getSecretID(),'secret_key' => $this->getSecretKey()));

    $call = $this->CallAPI("/CreateTransactionID", $msdata);

    return $call;
  }


  public function Check_Payment($data)
  {

    $msdata = array(
      'secret_id' => $this->getSecretID(),
      'secret_key' => $this->getSecretKey(),
      'transaction_ID' => $data,
    );

    $call = $this->CallAPI("/CheckPayment", $msdata);

    return $call;
  }

  public function Check_Transaction($tranID)
  {

    $hash = $this->getHash($tranID . $this->getTime(), $this->getSecretKey());
    $payment_data = array(
      'secreteID' => $this->getSecretID(),
      'transactionID' => $tranID,
      'timeHash' => $this->getTime(),
      'hash' => $hash
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://www.moneyspace.net/merchantapi/v1/findbytransaction/obj");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $data = $payment_data;

    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $output = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
    return $output;
  }

  public function Check_OrderID($data)
  {


    $msdata = array(
      'secret_id' => $this->getSecretID(),
      'secret_key' => $this->getSecretKey(),
      'order_id' => $data,
    );

    $call = $this->CallAPI("/CheckOrderID", $msdata);

    return $call;
  }

  public function GetWebhook()
  {

    $status = $_POST["status"];
    $amount = $_POST["amount"];
    $orderid = $_POST["orderid"];
    $hash = $_POST["hash"];

    $transactionID = ($status == "OK" ? $_POST["transactionID"] : $_POST["transectionID"]);
    $hash_for_verify = ($status == "OK" ? hash_hmac('sha256', $transactionID . $amount, $this->getSecretKey()) : hash_hmac('sha256', $transactionID . $amount . $status . $orderid, $this->getSecretKey()));


    if ($hash == $hash_for_verify) {

      return ["status_verify" => "pass", "data" => ["status" => $status, "transactionID" => $transactionID, "amount" => $amount, "orderid" => $orderid]];
    } else {

      return ["status_verify" => "fail"];
    }
  }
}
