﻿
  

# Requirements

- PHP 5.6+

- PHP knowledge

# Configuration

- เปิด moneyspace/config/config.php

- กรอก secret id , secret key

# Demo

[![Test on PHPSandbox](https://phpsandbox.io/img/brand/badge.png)](https://phpsandbox.io/n/ra1fh)

# Example

### ตัวอย่าง : การสร้าง Transaction ID และ เปิดเว็บชำระเงิน ( บัตรเครดิต )

- สถานะหลังชำระเงินที่ได้รับจาก Webhook มีดังนี้ : paysuccess , fail

#### เงื่อนไขยอมรับการขอคืนเงิน หรือการยกเลิกรายการการจ่ายเงิน ( **agreement** )

1. ข้าพเจ้ายอมรับว่าไม่สามารถขอคืนเงิน หรือยกเลิกรายการได้
2. ข้าพเจ้ายอมรับว่าไม่สามารถขอคืนเงินและเมื่อหากสินค้า / บริการมีปัญหาจะรีบติดต่อกลับ ภายใน 7 วัน หรือ ปฏิบัติตามนโยบายการคืนเงินของร้านค้า
3. ข้าพเจ้ายอมรับว่าไม่สามารถขอคืนเงินและเมื่อหากสินค้า / บริการมีปัญหาจะรีบติดต่อกลับ ภายใน 14 วัน หรือ ปฏิบัติตามนโยบายการคืนเงินของร้านค้า
4. ข้าพเจ้ายอมรับว่าไม่สามารถขอคืนเงินและเมื่อหากสินค้า / บริการมีปัญหาจะรีบติดต่อกลับ ภายใน 30 วัน หรือ ปฏิบัติตามนโยบายการคืนเงินของร้านค้า
5. ข้าพเจ้ายอมรับว่าไม่สามารถขอคืนเงินและเมื่อหากสินค้า / บริการมีปัญหาจะรีบติดต่อกลับ ภายใน 60 วัน หรือ ปฏิบัติตามนโยบายการคืนเงินของร้านค้า

```

<?php


require_once 'moneyspace/Api.php';

$api = new Api();

$msdata = $api->CreatePayment([
    'firstname' => "test",  // ชื่อลูกค้า
    'lastname' => "test", // สกุลลูกค้า
    'email' => "stepclick@outlook.com", // อีเมลล์เพื่อรับ ใบสำคัญรับเงิน (RECEIPT)
    'phone' => "0888888888",  // เบอร์โทรศัพท์
    'amount' => round("1",2), // จำนวนเงิน 
    'description' => "test123", // รายละเอียดสินค้า
    'address' => "test/test", // ที่อยู่ลูกค้า
    'message' => "test456", // ข้อความถึงร้านค้า
    'feeType' => "include", 
    'order_id' => "TEST".date("YmdHis"), // เลขที่ออเดอร์ ( ตัวอักษรภาษาอังกฤษพิมพ์ใหญ่ หรือตัวเลข สูงสุด 20 ตัว)
    "payment_type" => "card", // ประเภทการชำระเงิน ( card : บัตรเครดิต , qrnone : คิวอาร์โค๊ดพร้อมเพย์ )
    'success_Url' => "https://www.moneyspace.net?status=success111",  // เมื่อชำระเงินสำเร็จจะ redirect มายัง url
    'fail_Url' => "https://www.moneyspace.net?status=fail111", // เมื่อชำระเงินไม่สำเร็จจะ redirect มายัง url
    'cancel_Url' => "https://www.moneyspace.net?status=cancel111", // เมื่อชำระเงินไม่สำเร็จจะ redirect มายัง url
    "agreement" => 3
]);



$response = json_decode($msdata);


?>



<!DOCTYPE html>
<html>
	<head>
		<title>Demo Pay</title>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	</head>
	<body>
        <div align="center">
            <div id="moneyspace-payment" 
            template="2"
            lang="th"
            ms-title="Pay Now : <?=$response[0]->transaction_ID?>" 
            ms-key="<?=$response[0]->mskey?>" 
            description="false"
            color=""
		    bgcolor="">
        </div>
            
        </div>


		
	<script type="text/javascript" src="https://a.moneyspace.net/js/moneyspace_payment.js"></script></body>
</html>

```

*****

### ตัวอย่าง : การสร้าง Transaction ID และ เปิดเว็บชำระเงิน ( คิวอาร์โค้ด พร้อมเพย์ )

- สถานะหลังชำระเงินที่ได้รับจาก Webhook มีดังนี้ : paysuccess , fail

- คิวอาร์โค้ดต้องชำระเงินด้วยการสแกนภายใน 6 ชั่วโมง

```

<?php


require_once 'moneyspace/Api.php';

$api = new Api();

$msdata = $api->CreatePayment([
    'firstname' => "test",  // ชื่อลูกค้า
    'lastname' => "test", // สกุลลูกค้า
    'email' => "stepclick@outlook.com", // อีเมลล์เพื่อรับ ใบสำคัญรับเงิน (RECEIPT)
    'phone' => "0888888888",  // เบอร์โทรศัพท์
    'amount' => round("1",2), // จำนวนเงิน 
    'description' => "test123", // รายละเอียดสินค้า
    'address' => "test/test", // ที่อยู่ลูกค้า
    'message' => "test456", // ข้อความถึงร้านค้า
    'feeType' => "include", 
    'order_id' => "TEST".date("YmdHis"), // เลขที่ออเดอร์ ( ตัวอักษรภาษาอังกฤษพิมพ์ใหญ่ หรือตัวเลข สูงสุด 20 ตัว)
    "payment_type" => "qrnone", // ประเภทการชำระเงิน ( card : บัตรเครดิต , qrnone : คิวอาร์โค๊ดพร้อมเพย์ )
    'success_Url' => "https://www.moneyspace.net?status=success111",  // เมื่อชำระเงินสำเร็จจะ redirect มายัง url
    'fail_Url' => "https://www.moneyspace.net?status=fail111", // เมื่อชำระเงินไม่สำเร็จจะ redirect มายัง url
    'cancel_Url' => "https://www.moneyspace.net?status=cancel111", // เมื่อชำระเงินไม่สำเร็จจะ redirect มายัง url
    "agreement" => 3
]);



$response = json_decode($msdata);


?>



<!DOCTYPE html>
<html>
	<head>
		<title>Demo Pay</title>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	</head>
	<body>
        <div align="center">
            <div id="moneyspace-payment" 
            template="2"
            lang="th"
            ms-title="ชำระเงินด้วย QR Code : <?=$response[0]->transaction_ID?>" 
            ms-key="<?=$response[0]->mskey?>" 
            description="false"
            color=""
		    bgcolor="">
        </div>
            
        </div>


		
	<script type="text/javascript" src="https://a.moneyspace.net/js/moneyspace_payment.js"></script></body>
</html>

```

****

### ตัวอย่าง : การสร้าง Transaction ID และ เปิดเว็บชำระเงิน ( ผ่อนชำระรายเดือน 0%  )
- สถานะหลังชำระเงินที่ได้รับจาก Webhook มีดังนี้ : paysuccess , fail

#### ช่วงเดือนในการผ่อนชำระเงิน ( **startTerm , endTerm** )

**ประเภทบัตร (bankType)** | **ร้านค้ารับผิดชอบดอกเบี้ยรายเดือน (feeType : include)** | **ผู้ถือบัตรรับผิดชอบดอกเบี้ยรายเดือน ดอกเบี้ย 0.8% , 1% (feeType : exclude)** |
--- |--- | --- |
KTC | `3, 4, 5, 6, 7, 8, 9, 10` | `3, 4, 5, 6, 7, 8, 9, 10` ( 0.8% )
BAY | `3, 4, 6, 9, 10` | `3, 4, 6, 9, 10`  ( 0.8% )
FCY | `3, 4, 6, 9, 10` | `3, 4, 6, 9, 10, 12, 18, 24, 36`  ( 1% )

KTC : บัตรเคทีซี

BAY : บัตรกรุงศรี วีซ่า , บัตรเซ็นทรัล , บัตรเทสโก้โลตัส

FCY : บัตรกรุงศรีเฟิร์สช้อยส์ , บัตรโฮมโปร , บัตรเมกาโฮม


#### เงื่อนไขยอมรับการขอคืนเงิน หรือการยกเลิกรายการการจ่ายเงิน ( **agreement** )

1. ข้าพเจ้ายอมรับว่าไม่สามารถขอคืนเงิน หรือยกเลิกรายการได้
2. ข้าพเจ้ายอมรับว่าไม่สามารถขอคืนเงินและเมื่อหากสินค้า / บริการมีปัญหาจะรีบติดต่อกลับ ภายใน 7 วัน หรือ ปฏิบัติตามนโยบายการคืนเงินของร้านค้า
3. ข้าพเจ้ายอมรับว่าไม่สามารถขอคืนเงินและเมื่อหากสินค้า / บริการมีปัญหาจะรีบติดต่อกลับ ภายใน 14 วัน หรือ ปฏิบัติตามนโยบายการคืนเงินของร้านค้า
4. ข้าพเจ้ายอมรับว่าไม่สามารถขอคืนเงินและเมื่อหากสินค้า / บริการมีปัญหาจะรีบติดต่อกลับ ภายใน 30 วัน หรือ ปฏิบัติตามนโยบายการคืนเงินของร้านค้า
5. ข้าพเจ้ายอมรับว่าไม่สามารถขอคืนเงินและเมื่อหากสินค้า / บริการมีปัญหาจะรีบติดต่อกลับ ภายใน 60 วัน หรือ ปฏิบัติตามนโยบายการคืนเงินของร้านค้า




```
<?php


require_once 'moneyspace/Api.php';

$api = new Api();


$msdata = $api->CreatePayment([
    'firstname' => "test",
    'lastname' => "test",
    'email' => "stepclick@outlook.com",
    'phone' => "0888888888",
    'amount' => round("4800",2),
    'description' => "test123",
    'address' => "test/test",
    'message' => "test456",
    'feeType' => "include",
    'order_id' => "TEST-".date("YmdHis"),
    "payment_type" => "installment",
    'success_Url' => "https://www.moneyspace.net?status=success22",
    'fail_Url' => "https://www.moneyspace.net?status=fail22",
    'cancel_Url' => "https://www.moneyspace.net?status=cancel22",
    'agreement' => 1,
    'bankType' => "BAY",
    'startTerm' => "3",
    'endTerm' => "7"
]);


$response = json_decode($msdata);




?>


<!DOCTYPE html>
<html>
	<head>
		<title>Demo Pay</title>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	</head>
	<body>
        <div align="center">
        <div align="center">
            <div id="moneyspace-payment" 
            template="2"
            lang="th"
            ms-title="ผ่อนชำระเงิน : <?=$response[0]->transaction_ID?>" 
            ms-key="<?=$response[0]->mskey?>" 
            description="false"
            color=""
		    bgcolor="">
        </div>
        </div>

   
	<script type="text/javascript" src="https://a.moneyspace.net/js/moneyspace_payment.js"></script></body>
</html>



```

>  ***Note* : การตรวจสอบสถานะการขำระเงินต้องตรวจสอบด้วย order_id**

****

### ตัวอย่าง : การรับและตรวจสอบค่าจาก Webhook

- สถานะหลังชำระเงินที่ได้รับจาก Webhook มีดังนี้ : paysuccess , fail

```php

<?php 


require_once 'moneyspace/Api.php';

$api = new Api();

$GetWebhook = $api->GetWebhook(); 


if($GetWebhook["status_verify"] == "pass"){ // ตรวจข้อมูล Webhook ว่าถูกต้องหรือไม่

    // ตัวอย่างการรับค่าจาก webhook แล้วเขียนลงไฟล์ txt

    $status = $GetWebhook["data"]['status']; 
    $transactionID = $GetWebhook["data"]['transactionID']; 
    $amount = $GetWebhook["data"]['amount']; 
    $orderid = $GetWebhook["data"]['orderid']; 

    $txt = "status : ".$status." , transactionID : ".$transactionID. " , amount : ".$amount. " , orderid : ".$orderid;

    $file = 'webhook_data_'.date('dmY_Hsi').'.txt'; 
    $current = file_get_contents($file);
    file_put_contents($file, $txt);

}





?>



```

****

  

# Changelog

-  ##### 2022-05-29 : Add secret_key

-  ##### 2022-05-21 : Remove secret_key and add demo via phpsandbox

-  ##### 2020-07-10 : Updated payment methods to light box.

-  ##### 2020-04-10 : Added agreement parameter (Credit card)

-  ##### 2020-01-03 : Added installment payments

-  ##### 2019-12-25 : Updated QR Code Promptpay (qrnone)

-  ##### 2019-11-27 : Added new QR Code Promptpay (qrnone) and example code

-  ##### 2019-09-03 : Added example code

-  ##### 2019-09-03 : Added feeType

-  ##### 2019-06-24 : Added QR Code Payment flow

-  ##### 2019-06-07 : Added