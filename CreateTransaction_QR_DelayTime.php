<?php


require_once 'moneyspace/Api.php';

$api = new Api();

$msdata = $api->CreatePayment([
    'firstname' => "test",  // ชื่อลูกค้า
    'lastname' => "test", // สกุลลูกค้า
    'email' => "stepclick@outlook.com", // อีเมลล์เพื่อรับ ใบสำคัญรับเงิน (RECEIPT)
    'phone' => "0888888888",  // เบอร์โทรศัพท์
    'amount' => round("1",2), // จำนวนเงิน 
    'description' => "test123", // รายละเอียดสินค้า
    'address' => "test/test", // ที่อยู่ลูกค้า
    'message' => "test456", // ข้อความถึงร้านค้า
    'feeType' => "include", 
    'order_id' => "TEST".date("YmdHis"), // เลขที่ออเดอร์ ( ตัวอักษรภาษาอังกฤษพิมพ์ใหญ่ หรือตัวเลข สูงสุด 20 ตัว)
    "payment_type" => "qrnone", // ประเภทการชำระเงิน ( card : บัตรเครดิต , qrnone : คิวอาร์โค๊ดพร้อมเพย์ )
    'success_Url' => "https://www.moneyspace.net?status=success111",  // เมื่อชำระเงินสำเร็จจะ redirect มายัง url
    'fail_Url' => "https://www.moneyspace.net?status=fail111", // เมื่อชำระเงินไม่สำเร็จจะ redirect มายัง url
    'cancel_Url' => "https://www.moneyspace.net?status=cancel111", // เมื่อชำระเงินไม่สำเร็จจะ redirect มายัง url
    "agreement" => 3
]);

sleep(10); // set delay time to 10 sec after call create payment

$response = json_decode($msdata);
?>



<!DOCTYPE html>
<html>
	<head>
		<title>Demo Pay</title>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	</head>
	<body>
        <div align="center">
            <div id="moneyspace-payment" 
            template="2"
            lang="th"
            ms-title="ชำระเงินด้วย QR Code : <?=$response[0]->transaction_ID?>" 
            ms-key="<?=$response[0]->mskey?>" 
            description="false"
            color=""
		    bgcolor="">
        </div>
            
        </div>


		
	<script type="text/javascript" src="https://a.moneyspace.net/js/moneyspace_payment.js"></script></body>
</html>